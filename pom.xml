<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>de.humanfork.test.gitlab</groupId>
	<artifactId>gitlabtest</artifactId>
	<version>0.0.9-SNAPSHOT</version>

	<packaging>jar</packaging>
	<name>gitlabtest</name>

	<description>This is a test how to work with Gitlab.</description>
	<url>https://gitlab.com/RalphEng/gitlabtest</url>
	<inceptionYear>2019</inceptionYear>

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>https://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
			<comments>A business-friendly OSS license</comments>
		</license>
	</licenses>

	<organization>
		<name>Ralph Engelmann</name>
	</organization>

	<developers>
		<developer>
			<id>RalphEng</id>
			<name>Ralph Engelmann</name>
			<email>ralph@rshc.de</email>
			<url>https://gitlab.com/RalphEng</url>
			<properties>
				<picUrl>https://www.gravatar.com/avatar/5dc785b8d43eb411ca326c9cf9386753</picUrl>
			</properties>
		</developer>
	</developers>

	<scm>
		<connection>scm:git:https://gitlab.com/RalphEng/gitlabtest.git</connection>
		<developerConnection>scm:git:git@gitlab.com:RalphEng/gitlabtest.git</developerConnection>
		<url>https://gitlab.com/RalphEng/gitlabtest</url>
		<tag>HEAD</tag>
	</scm>

	<issueManagement>
		<system>GitLab</system>
		<url>https://gitlab.com/RalphEng/gitlabtest/issues</url>
	</issueManagement>

	<ciManagement>
		<system>GitLab CI/CD</system>
		<url>https://gitlab.com/RalphEng/gitlabtest/pipelines</url>
	</ciManagement>

	<distributionManagement>
		<site>
			<id>gitlab-pages-site</id>
			<name>Deployment through GitLab's site deployment plugin</name>
			<url>site/${project.version}</url>
		</site>
	</distributionManagement>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<java.version>1.8</java.version>
		<asciidoctorj.version>1.6.1</asciidoctorj.version>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.assertj</groupId>
				<artifactId>assertj-core</artifactId>
				<version>3.11.1</version>
			</dependency>

			<dependency>
				<groupId>org.junit</groupId>
				<artifactId>junit-bom</artifactId>
				<version>5.3.2</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<!-- ****** Test ***** -->
		<!-- junit -->
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-api</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-params</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-launcher</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.assertj</groupId>
			<artifactId>assertj-core</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-clean-plugin</artifactId>
					<version>3.1.0</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-site-plugin</artifactId>
					<version>3.7.1</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-project-info-reports-plugin</artifactId>
					<version>3.0.0</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-resources-plugin</artifactId>
					<version>3.1.0</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>3.8.0</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>2.22.1</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-source-plugin</artifactId>
					<version>3.0.1</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-jar-plugin</artifactId>
					<version>3.1.1</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-install-plugin</artifactId>
					<version>2.5.2</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>3.0.1</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-deploy-plugin</artifactId>
					<version>2.8.2</version>
				</plugin>
				
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-release-plugin</artifactId>
					<version>2.5.3</version>
					<configuration>
						<releaseProfiles>release</releaseProfiles>
					</configuration>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-gpg-plugin</artifactId>
					<version>1.6</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-enforcer-plugin</artifactId>
					<version>3.0.0-M2</version>
				</plugin>

				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>versions-maven-plugin</artifactId>
					<version>2.7</version>
					<configuration>
						<rulesUri>file:///${project.basedir}/src/maven/maven-version-rules.xml</rulesUri>
					</configuration>
				</plugin>

				<plugin>
					<groupId>com.github.ekryd.sortpom</groupId>
					<artifactId>sortpom-maven-plugin</artifactId>
					<version>2.10.0</version>
				</plugin>
			</plugins>
		</pluginManagement>

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-enforcer-plugin</artifactId>
				<executions>
					<execution>
						<id>enforce-maven</id>
						<goals>
							<goal>enforce</goal>
						</goals>
						<configuration>
							<rules>
								<requireMavenVersion>
									<version>3.3</version>
								</requireMavenVersion>
							</rules>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>com.github.ekryd.sortpom</groupId>
				<artifactId>sortpom-maven-plugin</artifactId>
				<configuration>
					<sortOrderFile>src/maven/pomStyle.xml</sortOrderFile>
					<encoding>UTF-8</encoding>
					<sortProperties>false</sortProperties>
					<expandEmptyElements>false</expandEmptyElements>
					<!-- use the system file encoding, svn will "normalize" it -->
					<keepBlankLines>true</keepBlankLines>
					<!-- nrOfIndentSpace=-1 indicates that a tab character should be used -->
					<nrOfIndentSpace>-1</nrOfIndentSpace>
					<createBackupFile>false</createBackupFile>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<configuration>
					<skipDeploy>true</skipDeploy>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>
				<reportSets>
					<!-- different report sets, the second one is just for the parent -->
					<reportSet>
						<id>project-info-report-every-module-first-part</id>
						<reports>
							<report>index</report>
						</reports>
					</reportSet>
					<reportSet>
						<id>project-info-report-parent-only</id>
						<reports>
							<report>team</report>
							<report>dependency-info</report>
							<report>scm</report>
							<report>distribution-management</report>
							<report>issue-management</report>
							<report>ci-management</report>
							<report>modules</report>
							<report>licenses</report>
						</reports>
						<inherited>false</inherited>
					</reportSet>
					<reportSet>
						<id>project-info-report-every-module-last-part</id>
						<reports>
							<report>dependencies</report>
							<report>dependency-convergence</report>
							<report>dependency-management</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
<!--
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>versions-maven-plugin</artifactId>
				<reportSets>
					<reportSet>
						<reports>
							<report>plugin-updates-report</report>
							<report>dependency-updates-report</report>
						</reports>
						<inherited>false</inherited>
					</reportSet>
					<reportSet>
						<!- - inherit no (an empty report set) to all child modules - ->
						<reports />
					</reportSet>
				</reportSets>
			</plugin>
-->
		</plugins>
	</reporting>

	<profiles>
		<profile>
			<id>release</id>
			<build>
				<plugins>					
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-source-plugin</artifactId>
						<executions>
							<execution>
								<goals>
									<goal>test-jar-no-fork</goal>
								</goals>
							</execution>
						</executions>
					</plugin>				
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-jar-plugin</artifactId>
						<executions>
							<execution>
								<goals>
									<goal>test-jar</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<executions>
							<execution>
								<id>sign-artifacts</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>									
								</goals>
							</execution>							
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>
